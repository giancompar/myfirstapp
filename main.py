from kivymd.app import MDApp
from kivy.lang import Builder

KV="""
Screen:
    MDRectangleFlatButton:
        text: "this is a button!"
        pos_hint: {"center_x": 0.5, "center_y": 0.5}
"""

class APP(MDApp):

    def build(self):
        self.title="thefirstapp"
        self.theme_cls.theme_style="Dark"
        self.theme_cls.primary_palette="Red"
        return Builder.load_string(KV)

APP.run()